package br.gov.anvisa.sngpc;

import org.junit.Test;

public class MD5EncoderTest {

    private MD5Encoder md5Encoder = new MD5Encoder();

    @Test
    public void testMD5() {
        String text = "<test></test>";
        String hash = md5Encoder.apply(text.getBytes());
        System.out.println(hash);
    }

}
