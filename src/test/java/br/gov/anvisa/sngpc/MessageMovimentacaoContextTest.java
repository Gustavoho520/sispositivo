package br.gov.anvisa.sngpc;


import br.gov.anvisa.sngpc.model.CtMsgMovimentacao;
import br.gov.anvisa.sngpc.model.ObjectFactory;
import br.gov.anvisa.sngpc.ws.EnviaArquivoSNGPC;
import com.github.javafaker.Faker;
import lombok.SneakyThrows;
import org.junit.Test;

import javax.xml.bind.JAXBElement;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Objects;

import static javax.xml.bind.JAXB.marshal;

public class MessageMovimentacaoContextTest {

    ObjectFactory factory = new ObjectFactory();

    Faker faker = new Faker(new Locale("pt-BR"));

    @SneakyThrows
    @Test
    public void test() {

        CtMsgMovimentacao ctMsgMovimentacao = factory
                .createCtMsgMovimentacao()
                .withCabecalho(factory
                        .createCtMsgMovimentacaoCabecalho()
                        .withCnpjEmissor(faker.code().gtin13())
                        .withCpfTransmissor(faker.code().gtin8())
                )
                .withCorpo(factory
                        .createCtMsgMovimentacaoCorpo()
                        .withMedicamentos(factory
                                .createCtMsgMovimentacaoCorpoMedicamentos()
                                .withEntradaMedicamentos(factory
                                        .createCtEntradaMedicamento()
                                        .withMedicamentoEntrada(factory
                                                .createCtMedicamentoEntrada()
                                                .withClasseTerapeutica(ClasseTerapeutica.ANTIMICROBIANO.getValue())
                                                .withNumeroLoteMedicamento(faker.color().hex())
                                                .withQuantidadeMedicamento(BigInteger.valueOf(2))
                                                .withRegistroMSMedicamento(faker.code().isbn10())
                                                .withUnidadeMedidaMedicamento(UnidadeMedidaMedicamento.FRASCOS.getValue())
                                        )
                                        .withNotaFiscalEntradaMedicamento(factory
                                                .createCtNotaFiscal()
                                                .withCnpjDestino(faker.code().gtin13())
                                                .withCnpjOrigem(faker.code().gtin13())
                                                .withNumeroNotaFiscal(BigInteger.valueOf(faker.number().randomNumber()))
                                                .withTipoOperacaoNotaFiscal(TipoOperacaoNotaFiscal.VENDA.getValue())
                                        )
                                )
                        )
                );

        JAXBElement<CtMsgMovimentacao> sngpc = factory.createMensagemSNGPC(ctMsgMovimentacao);

        StringWriter out = new StringWriter();

        marshal(sngpc, out);

        String file = out.toString();
        System.out.println(file);

        EnviaArquivoSNGPC arquivo = new br.gov.anvisa.sngpc.ws.ObjectFactory().createEnviaArquivoSNGPC();
        arquivo.setArq(file.getBytes());
        arquivo.setEmail("fdposiafdsa@fdsafdsa.com");
        arquivo.setSenha("fdsafdsafds");
        arquivo.setHashIdentificacao("fdsafdsafdsafsd");

        String md5 = "ouiboihnjk";
        String base64 = new String(file.getBytes());

        /*
        Sngpc sngpc1 = new Sngpc(new URL("http://homologacao.anvisa.gov.br/sngpc/webservice/sngpc.asmx"), new QName("sngpc"));
        String xmlResposta = sngpc1.getSngpcSoap().enviaArquivoSNGPC("fdposiafdsa@fdsafdsa.com", "fdafdsafs", base64.getBytes(), md5);
        */

        out.close();
    }

}
