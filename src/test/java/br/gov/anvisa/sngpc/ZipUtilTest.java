package br.gov.anvisa.sngpc;

import org.hamcrest.core.IsEqual;
import org.junit.Test;

import static org.junit.Assert.*;

public class ZipUtilTest {

    public static final String TEXT = "Sample text for zipping and dezipping test";
    private ZipUtil zipUtil = new ZipUtil();
    private DezipUtil dezipUtil = new DezipUtil();

    @Test
    public void testZip(){
        String ziped = zipUtil.apply(TEXT.getBytes());
        String deziped = dezipUtil.apply(ziped);
        assertThat(deziped, new IsEqual(TEXT));
    }

}
