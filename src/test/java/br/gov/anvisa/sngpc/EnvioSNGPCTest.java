package br.gov.anvisa.sngpc;

import br.gov.anvisa.sngpc.ws.EnviaArquivoSNGPC;
import br.gov.anvisa.sngpc.ws.Sngpc;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class EnvioSNGPCTest {

  @Test
  public void deveEnviarMovimentacao() throws IOException {
    String xml = IOUtils.resourceToString("movimentacao.xml", StandardCharsets.UTF_8, getClass().getClassLoader());
    System.out.println(xml);

    EnviaArquivoSNGPC arquivo = new br.gov.anvisa.sngpc.ws.ObjectFactory().createEnviaArquivoSNGPC();
    arquivo.setArq(xml.getBytes());
    arquivo.setEmail("fdposiafdsa@fdsafdsa.com");
    arquivo.setSenha("fdsafdsafds");
    arquivo.setHashIdentificacao("fdsafdsafdsafsd");

    String md5 = "ouiboihnjk";
    String base64 = new String(xml.getBytes());

    final Sngpc sngpc = new Sngpc();
    final String result = sngpc.getSngpcSoap().enviaArquivoSNGPC(arquivo.getEmail(), arquivo.getSenha(), xml.getBytes(), arquivo.getHashIdentificacao());
    System.out.println(result);

    final String resultConsulta = sngpc.getSngpcSoap().consultaDadosArquivoSNGPC(arquivo.getEmail(), arquivo.getSenha(), "00000000000000", arquivo.getHashIdentificacao());
    System.out.println(resultConsulta);
  }
}
