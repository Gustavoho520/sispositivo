package br.gov.anvisa.sngpc;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoOperacaoNotaFiscal {
    COMPRA("1"),
    TRANSFERENCIA("2"),
    VENDA("3");

    private final String value;
}
