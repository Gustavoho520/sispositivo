package br.gov.anvisa.sngpc;

import lombok.SneakyThrows;

import java.io.ByteArrayOutputStream;
import java.util.Base64;
import java.util.function.Function;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

public class ZipUtil implements Function<byte[], String> {

    private final Deflater deflater = new Deflater(Deflater.BEST_COMPRESSION);

    @SneakyThrows
    @Override
    public String apply(byte[] s) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        DeflaterOutputStream dos = new DeflaterOutputStream(os, deflater, 4 * 1024);
        dos.write(s);
        dos.flush();
        dos.close();
        return Base64.getEncoder().encodeToString(os.toByteArray());
    }

}
