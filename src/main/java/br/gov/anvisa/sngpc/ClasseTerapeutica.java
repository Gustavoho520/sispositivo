package br.gov.anvisa.sngpc;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ClasseTerapeutica {
    ANTIMICROBIANO("1"),
    SUJEITO_A_CONTROLE_ESPECIAL("2");

    private final String value;
}
