package br.gov.anvisa.sngpc;

import lombok.SneakyThrows;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.util.function.Function;

public class MD5Encoder implements Function<byte[], String> {
    @SneakyThrows
    @Override
    public String apply(byte[] s) {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(s);
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest);
    }
}
