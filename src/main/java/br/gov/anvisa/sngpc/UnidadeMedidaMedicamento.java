package br.gov.anvisa.sngpc;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UnidadeMedidaMedicamento {
    CAIXAS("1"),
    FRASCOS("2");

    private final String value;
}
