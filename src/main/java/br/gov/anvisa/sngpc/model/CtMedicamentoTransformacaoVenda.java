//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.2 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.09.04 at 02:07:56 PM WEST 
//


package br.gov.anvisa.sngpc.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ct_MedicamentoTransformacaoVenda complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ct_MedicamentoTransformacaoVenda"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="usoProlongado" type="{urn:sngpc-schema}st_SimNaoNull"/&gt;
 *         &lt;element name="registroMSMedicamento" type="{urn:sngpc-schema}st_RegistroMS"/&gt;
 *         &lt;element name="numeroLoteMedicamento" type="{urn:sngpc-schema}st_Lote"/&gt;
 *         &lt;element name="quantidadeDeInsumoPorUnidadeFarmacotecnica" type="{urn:sngpc-schema}st_QuantidadeDeInsumoPorUnidadeFarmacotecnica"/&gt;
 *         &lt;element name="unidadeDeMedidaDoInsumo" type="{urn:sngpc-schema}st_TipoUnidadeInsumo"/&gt;
 *         &lt;element name="unidadeFarmacotecnica" type="{urn:sngpc-schema}st_TipoUnidadeFarmacotecnica"/&gt;
 *         &lt;element name="quantidadeDeUnidadesFarmacotecnicas" type="{urn:sngpc-schema}st_QuantidadeDeUnidadesFarmacotecnicas"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ct_MedicamentoTransformacaoVenda", propOrder = {
    "usoProlongado",
    "registroMSMedicamento",
    "numeroLoteMedicamento",
    "quantidadeDeInsumoPorUnidadeFarmacotecnica",
    "unidadeDeMedidaDoInsumo",
    "unidadeFarmacotecnica",
    "quantidadeDeUnidadesFarmacotecnicas"
})
public class CtMedicamentoTransformacaoVenda {

    @XmlElement(required = true)
    protected String usoProlongado;
    @XmlElement(required = true)
    protected String registroMSMedicamento;
    @XmlElement(required = true)
    protected String numeroLoteMedicamento;
    @XmlElement(required = true)
    protected BigDecimal quantidadeDeInsumoPorUnidadeFarmacotecnica;
    @XmlElement(required = true)
    protected String unidadeDeMedidaDoInsumo;
    @XmlElement(required = true)
    protected BigInteger unidadeFarmacotecnica;
    @XmlElement(required = true)
    protected BigDecimal quantidadeDeUnidadesFarmacotecnicas;

    /**
     * Gets the value of the usoProlongado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsoProlongado() {
        return usoProlongado;
    }

    /**
     * Sets the value of the usoProlongado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsoProlongado(String value) {
        this.usoProlongado = value;
    }

    /**
     * Gets the value of the registroMSMedicamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistroMSMedicamento() {
        return registroMSMedicamento;
    }

    /**
     * Sets the value of the registroMSMedicamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistroMSMedicamento(String value) {
        this.registroMSMedicamento = value;
    }

    /**
     * Gets the value of the numeroLoteMedicamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroLoteMedicamento() {
        return numeroLoteMedicamento;
    }

    /**
     * Sets the value of the numeroLoteMedicamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroLoteMedicamento(String value) {
        this.numeroLoteMedicamento = value;
    }

    /**
     * Gets the value of the quantidadeDeInsumoPorUnidadeFarmacotecnica property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQuantidadeDeInsumoPorUnidadeFarmacotecnica() {
        return quantidadeDeInsumoPorUnidadeFarmacotecnica;
    }

    /**
     * Sets the value of the quantidadeDeInsumoPorUnidadeFarmacotecnica property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQuantidadeDeInsumoPorUnidadeFarmacotecnica(BigDecimal value) {
        this.quantidadeDeInsumoPorUnidadeFarmacotecnica = value;
    }

    /**
     * Gets the value of the unidadeDeMedidaDoInsumo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnidadeDeMedidaDoInsumo() {
        return unidadeDeMedidaDoInsumo;
    }

    /**
     * Sets the value of the unidadeDeMedidaDoInsumo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnidadeDeMedidaDoInsumo(String value) {
        this.unidadeDeMedidaDoInsumo = value;
    }

    /**
     * Gets the value of the unidadeFarmacotecnica property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUnidadeFarmacotecnica() {
        return unidadeFarmacotecnica;
    }

    /**
     * Sets the value of the unidadeFarmacotecnica property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUnidadeFarmacotecnica(BigInteger value) {
        this.unidadeFarmacotecnica = value;
    }

    /**
     * Gets the value of the quantidadeDeUnidadesFarmacotecnicas property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQuantidadeDeUnidadesFarmacotecnicas() {
        return quantidadeDeUnidadesFarmacotecnicas;
    }

    /**
     * Sets the value of the quantidadeDeUnidadesFarmacotecnicas property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQuantidadeDeUnidadesFarmacotecnicas(BigDecimal value) {
        this.quantidadeDeUnidadesFarmacotecnicas = value;
    }

    public CtMedicamentoTransformacaoVenda withUsoProlongado(String value) {
        setUsoProlongado(value);
        return this;
    }

    public CtMedicamentoTransformacaoVenda withRegistroMSMedicamento(String value) {
        setRegistroMSMedicamento(value);
        return this;
    }

    public CtMedicamentoTransformacaoVenda withNumeroLoteMedicamento(String value) {
        setNumeroLoteMedicamento(value);
        return this;
    }

    public CtMedicamentoTransformacaoVenda withQuantidadeDeInsumoPorUnidadeFarmacotecnica(BigDecimal value) {
        setQuantidadeDeInsumoPorUnidadeFarmacotecnica(value);
        return this;
    }

    public CtMedicamentoTransformacaoVenda withUnidadeDeMedidaDoInsumo(String value) {
        setUnidadeDeMedidaDoInsumo(value);
        return this;
    }

    public CtMedicamentoTransformacaoVenda withUnidadeFarmacotecnica(BigInteger value) {
        setUnidadeFarmacotecnica(value);
        return this;
    }

    public CtMedicamentoTransformacaoVenda withQuantidadeDeUnidadesFarmacotecnicas(BigDecimal value) {
        setQuantidadeDeUnidadesFarmacotecnicas(value);
        return this;
    }

}
