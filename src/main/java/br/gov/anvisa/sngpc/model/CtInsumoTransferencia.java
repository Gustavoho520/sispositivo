//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.2 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.09.04 at 02:07:56 PM WEST 
//


package br.gov.anvisa.sngpc.model;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ct_InsumoTransferencia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ct_InsumoTransferencia"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="insumoTransferencia" type="{urn:sngpc-schema}ct_Insumo"/&gt;
 *         &lt;element name="quantidadeInsumoTransferencia" type="{urn:sngpc-schema}st_QuantidadeInsumo"/&gt;
 *         &lt;element name="tipoUnidadeTransferencia" type="{urn:sngpc-schema}st_TipoUnidadeInsumo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ct_InsumoTransferencia", propOrder = {
    "insumoTransferencia",
    "quantidadeInsumoTransferencia",
    "tipoUnidadeTransferencia"
})
public class CtInsumoTransferencia {

    @XmlElement(required = true)
    protected CtInsumo insumoTransferencia;
    @XmlElement(required = true)
    protected BigDecimal quantidadeInsumoTransferencia;
    @XmlElement(required = true)
    protected String tipoUnidadeTransferencia;

    /**
     * Gets the value of the insumoTransferencia property.
     * 
     * @return
     *     possible object is
     *     {@link CtInsumo }
     *     
     */
    public CtInsumo getInsumoTransferencia() {
        return insumoTransferencia;
    }

    /**
     * Sets the value of the insumoTransferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtInsumo }
     *     
     */
    public void setInsumoTransferencia(CtInsumo value) {
        this.insumoTransferencia = value;
    }

    /**
     * Gets the value of the quantidadeInsumoTransferencia property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQuantidadeInsumoTransferencia() {
        return quantidadeInsumoTransferencia;
    }

    /**
     * Sets the value of the quantidadeInsumoTransferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQuantidadeInsumoTransferencia(BigDecimal value) {
        this.quantidadeInsumoTransferencia = value;
    }

    /**
     * Gets the value of the tipoUnidadeTransferencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoUnidadeTransferencia() {
        return tipoUnidadeTransferencia;
    }

    /**
     * Sets the value of the tipoUnidadeTransferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoUnidadeTransferencia(String value) {
        this.tipoUnidadeTransferencia = value;
    }

    public CtInsumoTransferencia withInsumoTransferencia(CtInsumo value) {
        setInsumoTransferencia(value);
        return this;
    }

    public CtInsumoTransferencia withQuantidadeInsumoTransferencia(BigDecimal value) {
        setQuantidadeInsumoTransferencia(value);
        return this;
    }

    public CtInsumoTransferencia withTipoUnidadeTransferencia(String value) {
        setTipoUnidadeTransferencia(value);
        return this;
    }

}
