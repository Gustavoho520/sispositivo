//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.2 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.09.04 at 02:07:56 PM WEST 
//


package br.gov.anvisa.sngpc.model;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ct_MedicamentoEntrada complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ct_MedicamentoEntrada"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="classeTerapeutica" type="{urn:sngpc-schema}st_classeTerapeutica"/&gt;
 *         &lt;element name="registroMSMedicamento" type="{urn:sngpc-schema}st_RegistroMS"/&gt;
 *         &lt;element name="numeroLoteMedicamento" type="{urn:sngpc-schema}st_Lote"/&gt;
 *         &lt;element name="quantidadeMedicamento" type="{urn:sngpc-schema}st_QuantidadeMedicamento"/&gt;
 *         &lt;element name="unidadeMedidaMedicamento" type="{urn:sngpc-schema}st_UnidadeMedidaMedicamento"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ct_MedicamentoEntrada", propOrder = {
    "classeTerapeutica",
    "registroMSMedicamento",
    "numeroLoteMedicamento",
    "quantidadeMedicamento",
    "unidadeMedidaMedicamento"
})
public class CtMedicamentoEntrada {

    @XmlElement(required = true)
    protected String classeTerapeutica;
    @XmlElement(required = true)
    protected String registroMSMedicamento;
    @XmlElement(required = true)
    protected String numeroLoteMedicamento;
    @XmlElement(required = true)
    protected BigInteger quantidadeMedicamento;
    @XmlElement(required = true)
    protected String unidadeMedidaMedicamento;

    /**
     * Gets the value of the classeTerapeutica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClasseTerapeutica() {
        return classeTerapeutica;
    }

    /**
     * Sets the value of the classeTerapeutica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClasseTerapeutica(String value) {
        this.classeTerapeutica = value;
    }

    /**
     * Gets the value of the registroMSMedicamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistroMSMedicamento() {
        return registroMSMedicamento;
    }

    /**
     * Sets the value of the registroMSMedicamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistroMSMedicamento(String value) {
        this.registroMSMedicamento = value;
    }

    /**
     * Gets the value of the numeroLoteMedicamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroLoteMedicamento() {
        return numeroLoteMedicamento;
    }

    /**
     * Sets the value of the numeroLoteMedicamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroLoteMedicamento(String value) {
        this.numeroLoteMedicamento = value;
    }

    /**
     * Gets the value of the quantidadeMedicamento property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantidadeMedicamento() {
        return quantidadeMedicamento;
    }

    /**
     * Sets the value of the quantidadeMedicamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantidadeMedicamento(BigInteger value) {
        this.quantidadeMedicamento = value;
    }

    /**
     * Gets the value of the unidadeMedidaMedicamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnidadeMedidaMedicamento() {
        return unidadeMedidaMedicamento;
    }

    /**
     * Sets the value of the unidadeMedidaMedicamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnidadeMedidaMedicamento(String value) {
        this.unidadeMedidaMedicamento = value;
    }

    public CtMedicamentoEntrada withClasseTerapeutica(String value) {
        setClasseTerapeutica(value);
        return this;
    }

    public CtMedicamentoEntrada withRegistroMSMedicamento(String value) {
        setRegistroMSMedicamento(value);
        return this;
    }

    public CtMedicamentoEntrada withNumeroLoteMedicamento(String value) {
        setNumeroLoteMedicamento(value);
        return this;
    }

    public CtMedicamentoEntrada withQuantidadeMedicamento(BigInteger value) {
        setQuantidadeMedicamento(value);
        return this;
    }

    public CtMedicamentoEntrada withUnidadeMedidaMedicamento(String value) {
        setUnidadeMedidaMedicamento(value);
        return this;
    }

}
