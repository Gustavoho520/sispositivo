
package br.gov.anvisa.sngpc.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EnviaArquivoSNGPCResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enviaArquivoSNGPCResult"
})
@XmlRootElement(name = "EnviaArquivoSNGPCResponse")
public class EnviaArquivoSNGPCResponse {

    @XmlElement(name = "EnviaArquivoSNGPCResult")
    protected String enviaArquivoSNGPCResult;

    /**
     * Gets the value of the enviaArquivoSNGPCResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnviaArquivoSNGPCResult() {
        return enviaArquivoSNGPCResult;
    }

    /**
     * Sets the value of the enviaArquivoSNGPCResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnviaArquivoSNGPCResult(String value) {
        this.enviaArquivoSNGPCResult = value;
    }

}
