package br.gov.anvisa.sngpc;

import lombok.SneakyThrows;

import java.io.ByteArrayOutputStream;
import java.util.Base64;
import java.util.function.Function;
import java.util.zip.Inflater;
import java.util.zip.InflaterOutputStream;

public class DezipUtil implements Function<String, String> {

    private Inflater inflater = new Inflater();

    @SneakyThrows
    @Override
    public String apply(String s) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        InflaterOutputStream ios = new InflaterOutputStream(os, inflater, 4 * 1024);
        ios.write(Base64.getDecoder().decode(s.getBytes()));
        ios.flush();
        ios.close();
        return os.toString();
    }
}
